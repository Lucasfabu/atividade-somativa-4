import React from "react";

import { logo } from "../assets";

const Hero = () => {
  return (
    <header className='w-full flex justify-center items-center flex-col'>
      <nav className='flex justify-between items-center w-full mb-10 pt-3'>
        <img src={logo} alt='sumz_logo' className='w-28 object-contain' />

        <button
          type='button'
          onClick={() =>
            window.open("https://gitlab.com/Lucasfabu/atividade-somativa-4", "_blank")
          }
          className='black_btn'
        >
          GitLab
        </button>
      </nav>

      <h1 className='head_text'>
        Faça resumos de artigos com <br className='max-md:hidden' />
        <span className='orange_gradient '>Inteligência Artificial</span>
      </h1>
      <h2 className='desc'>
        Agilize o seu trabalho com o uso do GTP-4, uma IA desenvolvida pela OpenAI.
        Com essa ferramenta é possível resumir artigos inteiros em apenas alguns segundos
      </h2>
    </header>
  );
};

export default Hero;
